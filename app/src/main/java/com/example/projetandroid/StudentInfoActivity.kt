package com.example.projetandroid

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.widget.TextView

class StudentInfoActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_student_info)
        showBtnBack()
        intent.getStringExtra("title")?.let { setHeaderTitle(it) }
        intent.getStringExtra("lastName")?.let { setLastName(it) }
        intent.getStringExtra("firstName")?.let { setFirstName(it) }
        intent.getStringExtra("email")?.let { setEmail(it) }

        val url_epsi = findViewById<TextView>(R.id.url_epsi)
        val webview_epsi: WebView = findViewById(R.id.webview_epsi)
        url_epsi.setOnClickListener(View.OnClickListener {
            webview_epsi.loadUrl("http://www.epsi.fr/")
        })

    }
}