package com.example.projetandroid

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button

class MainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val buttonZone1: Button = findViewById(R.id.buttonZone1)
        val buttonZone2: Button = findViewById(R.id.buttonZone2)

        buttonZone1.setOnClickListener(View.OnClickListener {
            val newIntent= Intent(application,GroupInfoActivity::class.java)
            newIntent.putExtra("title",getString(R.string.txt_title_infos))
            startActivity(newIntent)

        })
        buttonZone2.setOnClickListener(View.OnClickListener {
            val newIntent= Intent(application,CategoryActivity::class.java)
            newIntent.putExtra("title",getString(R.string.txt_title_rayons))
            startActivity(newIntent)

        })
    }
}