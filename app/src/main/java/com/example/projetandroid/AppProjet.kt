package com.example.projetandroid

import android.app.Application
import android.widget.Toast

class AppProjet : Application(){

    fun showToast(txt : String){
        Toast.makeText(this,txt,Toast.LENGTH_SHORT).show()
    }

}