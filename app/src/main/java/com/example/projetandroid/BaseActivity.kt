package com.example.projetandroid

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.widget.ImageView
import android.widget.TextView

open class BaseActivity : AppCompatActivity() {

    fun showBtnBack(){
        val imageViewBack=findViewById<ImageView>(R.id.imageViewBack)
        imageViewBack.visibility= View.VISIBLE
        imageViewBack.setOnClickListener(View.OnClickListener {
            finish()
        })
    }

    fun setHeaderTitle(txt : String){
        val textViewTitle = findViewById<TextView>(R.id.textViewTitle)
        textViewTitle.text = txt
    }

    fun setLastName(lastName : String){
        val txt_lastName = findViewById<TextView>(R.id.txt_lastName)
        txt_lastName.text = lastName
    }
    fun setFirstName(firstName : String){
        val txt_firstName = findViewById<TextView>(R.id.txt_firstName)
        txt_firstName.text = firstName
    }
    fun setEmail(email : String){
        val txt_email = findViewById<TextView>(R.id.txt_email)
        txt_email.text = email
    }
    fun setProductDetailTitle(title : String){
        val textViewTitle = findViewById<TextView>(R.id.textViewTitle)
        textViewTitle.text = title
        textViewTitle.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18F);

    }



}