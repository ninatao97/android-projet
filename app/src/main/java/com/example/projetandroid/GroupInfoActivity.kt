package com.example.projetandroid

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button

class GroupInfoActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group_info)
        val buttonStudent1: Button = findViewById(R.id.button_student1)
        val buttonStudent2: Button = findViewById(R.id.button_student2)


        showBtnBack()
        intent.getStringExtra("title")?.let { setHeaderTitle(it) }

        buttonStudent1.setOnClickListener(View.OnClickListener {
            val newIntent= Intent(application,StudentInfoActivity::class.java)
            newIntent.putExtra("title",getString(R.string.student1_lastname))
            newIntent.putExtra("lastName",getString(R.string.student1_lastname))
            newIntent.putExtra("firstName",getString(R.string.student1_firstname))
            newIntent.putExtra("email",getString(R.string.student1_email))
            startActivity(newIntent)
        })
        buttonStudent2.setOnClickListener(View.OnClickListener {
            val newIntent= Intent(application,StudentInfoActivity::class.java)
            newIntent.putExtra("title",getString(R.string.student2_lastname))
            newIntent.putExtra("lastName",getString(R.string.student2_lastname))
            newIntent.putExtra("firstName",getString(R.string.student2_firstname))
            newIntent.putExtra("email",getString(R.string.student2_email))
            startActivity(newIntent)
        })
    }
}